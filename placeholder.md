# 日（に）本（ほん）語（ご）を書（か）いて見（み）ましょう
This is a custom markdown editor with syntax for the ruby html tags.
### Usage
1. You must use the （）characters that you type with your **IME**.
2. You can only include hiragana inside these parens.

Here are some examples
* 日（にち）・**works**.
* 日(にち)・**incorrect!** The parentheses are the wrong kind.
* 日（ニチ）・**incorrect!**  You need to use hiragana.

### Resources
* [showdown](https://github.com/showdownjs/showdown) 
* [showdown-furigana-extension](https://github.com/vincent314/showdown-furigana-extension)